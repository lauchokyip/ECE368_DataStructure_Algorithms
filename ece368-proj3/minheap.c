#include <stdio.h>
#include <stdlib.h>
#include "minheap.h"
#include "hash.h"
#include "map.h"



void swap(Vertex **v1, Vertex **v2){
    //swap vertex and the array index
    Vertex *temp = *v1;
    unsigned long int temp_val = (*v1)->ptr_arr_index;
    (*v1)->ptr_arr_index  = (*v2)->ptr_arr_index;
    (*v2)->ptr_arr_index  = temp_val;
    *v1 = *v2;
    *v2 = temp;

}


minHeap *initialize_minHeap(Vertex *input_vertexs){

    minHeap *test_minheap = malloc(sizeof(*test_minheap));
        if(test_minheap == NULL){
            fprintf(stderr,"failed to malloc\n");
            return NULL;
        }
    //array of pointer that point to each array
    test_minheap->vertexs = malloc(sizeof(*(test_minheap->vertexs))* global_vertex_size);
        if(test_minheap->vertexs == NULL){
        fprintf(stderr,"failed to malloc\n");
        return NULL;
        }
    //point to everything
    for(unsigned long int i = 0; i < global_vertex_size; i++){
        test_minheap->vertexs[i] = &(input_vertexs[i]);
    }
    //set the heap size
    test_minheap->size = global_vertex_size;
    return test_minheap;
}

void heapify (minHeap *minHeap_v, Vertex **vertexs, unsigned long int i ){

    //if left child is smaller than parent
     unsigned long int smallest = 0 ;
    if(LCHILD(i) < minHeap_v->size && 
       vertexs[LCHILD(i)]->shortest_dist_from_query < vertexs[i]->shortest_dist_from_query){
           smallest = LCHILD(i);
       }
       else
       {
           smallest = i;
       }
    //if right child is smaller than left child
    if(RCHILD(i) < minHeap_v->size &&
       vertexs[RCHILD(i)]->shortest_dist_from_query < vertexs[smallest]->shortest_dist_from_query ){
           smallest = RCHILD(i);
       }
    //if it's smalles then swap with it's children
       if(smallest != i){
           swap( &(vertexs[i]), &(vertexs[smallest]));
           heapify(minHeap_v,vertexs,smallest);
       }

}

void upward_heapify(minHeap *min_heap, Vertex *vertexs, unsigned long int adj_vertex_index){
    unsigned long int ptr_arr_index = vertexs[adj_vertex_index].ptr_arr_index;
    //while the node is not in place, swap with it's parent
    while(ptr_arr_index != 0  && min_heap->vertexs[PARENT(ptr_arr_index)]->shortest_dist_from_query > 
    min_heap->vertexs[ptr_arr_index]->shortest_dist_from_query){
        swap(&(min_heap->vertexs[PARENT(ptr_arr_index)]), &(min_heap->vertexs[ptr_arr_index]));
        ptr_arr_index = PARENT(ptr_arr_index); 
    }
}


unsigned long int extractMin(minHeap *min_heap){
    //the root is the minimum so swap it with the end of the heap
    swap(&(min_heap->vertexs[0]), &(min_heap->vertexs[(min_heap->size)-1])); 
    //decrease the size
    min_heap->size -= 1;
    //extract the root, so have to heapify again
    heapify( min_heap,min_heap->vertexs, 0);
    //give the shortest distance index
    return search_hash(( min_heap->vertexs[(min_heap->size)]->self_vertex_index));
}


void free_heap(minHeap * min_heap){
    free(min_heap->vertexs);
    free(min_heap);
}