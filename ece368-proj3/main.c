#include <stdio.h>
#include <stdlib.h>
#include "map.h"
#include "minheap.h"
#include "hash.h"

int main(int argc, char **argv){
    if(argc != 3){
        fprintf(stderr,"Please insert enough input\n");
        return EXIT_FAILURE;
    }
    Vertex *vertexs_v = build_adjacent(argv[1]);
   
    FILE *fptr = fopen(argv[2],"r");
    if(fptr == NULL){
        fprintf(stderr,"Query file fails to be opened\n");
    }
    unsigned long int query_amount = 0;
    unsigned long int start = 0;
    unsigned long int end = 0;

    fscanf(fptr,"%lu",&query_amount);
    for (unsigned i = 0 ; i < query_amount; i++){
        fscanf(fptr,"%lu %lu",&start,&end);
        dijkstra(vertexs_v, start, end);
    }


    fclose(fptr);
    
    free_hash(hash_v);
    free_vertexs(vertexs_v);


    return EXIT_SUCCESS;
}   


