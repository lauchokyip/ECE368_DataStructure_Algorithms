#ifndef HASH_H
#define HASH_H

typedef struct hash{
    unsigned long int *hash_table;
}hash;

//global variable for hash table
hash *hash_v;


void initialize_hash_table(hash *hash_v);
void print_hash(hash *hash_v);
unsigned long int insert_hash(unsigned long int val);
unsigned long int search_hash(unsigned long int val);
void free_hash(hash *hash_v);
#endif
