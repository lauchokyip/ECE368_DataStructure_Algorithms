#ifndef MINHEAP_H
#define MINHEAP_H
#include "map.h"

#define LCHILD(x) 2 * x + 1
#define RCHILD(x) 2 * x + 2
#define PARENT(x) (x - 1) / 2

typedef struct minHeap{
    Vertex **vertexs;
    unsigned long int size;
}minHeap;

minHeap *initialize_minHeap(Vertex *input_vertexs);

void swap(Vertex **v1, Vertex **v2);

void heapify (minHeap *minHeap_v, Vertex **vertexs, unsigned long int i );

unsigned long int extractMin(minHeap *min_heap);

void upward_heapify(minHeap *min_heap, Vertex *vertexs, unsigned long int adj_vertex_index);

void free_heap(minHeap * min_heap);

#endif