#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <limits.h>
#include "map.h"
#include "minheap.h"
#include "list.h"
#include "hash.h"



static double square_distance(Vertex *v1, Vertex *v2){
    signed long int dx = (v1->x_coor) -(v2->x_coor);
    signed long int dy =  (v1->y_coor) - (v2->y_coor);
    signed long int distance = dx*dx + dy*dy;
    return sqrt(distance);
}


static void relax(Vertex *u, Vertex *v){
    unsigned long int check_distance =  u->shortest_dist_from_query + square_distance(u,v);
    if ((v->shortest_dist_from_query) > check_distance){
        v->shortest_dist_from_query = check_distance;
        v->shortest_prev_vertex_index = search_hash(u->self_vertex_index);
    }

}

static void print_query(Vertex *vertexs, unsigned long int start, unsigned long int end){
    unsigned long int check_start = search_hash(start);
    unsigned long int check_end = search_hash(end);

    if(check_start == ULONG_MAX || check_end == ULONG_MAX){
        printf("INF\n");
        printf("%lu %lu\n", start, end);
        return;
    }
    
    //so it's in range after checking it from hash table
    //overwrite the start and end value
    start = check_start;
    end = check_end;

    //if the shortest dist is infinity
    if(vertexs[end].shortest_dist_from_query == ULONG_MAX){
        printf("INF\n");
        printf("%lu %lu\n", start, end);
        return;
    }

    unsigned long int *print_arr = malloc(sizeof(*print_arr) * (global_vertex_size+1) );
    if(print_arr == NULL){
        fprintf(stderr,"malloc error\n");
        return;
    }
    unsigned long int running_index = end;
    unsigned long int i = 1;
    printf("%lu\n",vertexs[end].shortest_dist_from_query);
    while( running_index != start && i < global_vertex_size){
        print_arr[i] = vertexs[running_index].self_vertex_index;
        running_index = vertexs[running_index].shortest_prev_vertex_index;
        i++;
    }
    //store the last one
    print_arr[i] = vertexs[start].self_vertex_index;

    while(i > 0){
        printf("%lu ", print_arr[i]);
        i--;
    }
    printf("\n");
    free(print_arr);

}

void print_vertexs(Vertex *vertexs){

    for (unsigned long int i = 0; i< global_vertex_size; i++){
        printf("%lu: ",i);
        print_list(vertexs[i].adj_vertex_index_list_head);
    }
}

Vertex *build_adjacent(char *input_filename){
    
    unsigned long int num_of_vertex;
    unsigned long int num_of_edges;
    unsigned long int vertex_index;
    unsigned long int vertex_index1;
    unsigned long int x,y;
    //open the file
    FILE *fptr = fopen(input_filename,"r");
    if(fptr == NULL){
        fprintf(stderr,"cannot open input file\n");
    }

    //scan the first two number
    fscanf(fptr, "%lu %lu",&num_of_vertex,&num_of_edges);
    
    //allocate all vertex
    Vertex *vertexs = malloc(num_of_vertex * sizeof(*vertexs));
        if(vertexs == NULL){
        fprintf(stderr,"malloc error\n");
        return NULL;
        }
    global_vertex_size = num_of_vertex;

    //initialize hash table
    hash_v = malloc(sizeof(*hash_v));
    if(hash_v == NULL){
        fprintf(stderr, "malloc failed\n");
        return NULL;
    }
    hash_v->hash_table = malloc(sizeof (*(hash_v->hash_table)) * num_of_vertex);
    if(hash_v->hash_table == NULL){
        fprintf(stderr, "malloc failed\n");
        return NULL;
    }
    initialize_hash_table(hash_v);
  
   // run a for loop until all the coordinates is inserted
    for (unsigned long int i = 0 ; i < num_of_vertex ; i ++){
        fscanf(fptr, "%lu %lu %lu",&vertex_index,&x,&y);
        unsigned long int pos = insert_hash(vertex_index);
        if(pos == ULONG_MAX){
            fprintf(stderr,"insert hash error\n");
        }
        vertexs[pos].self_vertex_index = vertex_index;
        vertexs[pos].x_coor = x;
        vertexs[pos].y_coor = y;
        vertexs[pos].shortest_dist_from_query = ULONG_MAX;
        vertexs[pos].shortest_prev_vertex_index = NIL;
        node *head = malloc(sizeof(*head));
        if(head == NULL){
            fprintf(stderr, "malloc error\n");
        }
        head->next = NULL;
        vertexs[pos].adj_vertex_index_list_head = head;
    }

    // get all edges
    for (int i = 0; i < num_of_edges; i++){
        fscanf(fptr, "%lu %lu",&vertex_index,&vertex_index1);

        Vertex left_vertex = vertexs[search_hash(vertex_index)];
        Vertex right_vertex = vertexs[search_hash(vertex_index1)];

        scan_and_insert(left_vertex.adj_vertex_index_list_head, vertex_index1);
        scan_and_insert(right_vertex.adj_vertex_index_list_head, vertex_index);

    }

    fclose(fptr);
    return vertexs;
}

void free_vertexs(Vertex *vertexs){
    for (unsigned long int i = 0 ; i < global_vertex_size; i++){
        free_all_nodes(vertexs[i].adj_vertex_index_list_head);
    }
    free(vertexs);
}


void dijkstra(Vertex *vertexs, unsigned long int start, unsigned long int end){
    //remember the start so it can use in print to check if it's valid
    unsigned long int unhash_start = start;
    unsigned long int unhash_end = end;
    //hash the value
    start = search_hash(start);
    end = search_hash(end);
    // initialize single source
    for (unsigned long int i = 0; i < global_vertex_size; i ++){
        if(i != start){
            vertexs[i].shortest_dist_from_query = ULONG_MAX;
            vertexs[i].shortest_prev_vertex_index = NIL;
            vertexs[i].isVisited = false;
        }
        else{
            // distance of start node from start node
            vertexs[start].shortest_dist_from_query = 0;
        }
        
    }

    minHeap *minHeap_v = initialize_minHeap(vertexs);
    
    unsigned long int running_smallest_vertex_ptr = start;
    
    //build heap
    for (unsigned long int i = (minHeap_v->size/2) ; i > 0; i--){
        heapify( minHeap_v, minHeap_v->vertexs, (i-1) );
    }

    // build array index 
    for (unsigned long int i = 0 ; i< global_vertex_size; i++){
       unsigned long int index =  search_hash(minHeap_v->vertexs[i]->self_vertex_index);
       vertexs[index].ptr_arr_index = i ; 
    }

    //point to source
    Vertex *current_vertex =  &(vertexs[start]);

    // while we didn't reach the goal and hasn't visited all the node
    while( current_vertex->self_vertex_index != unhash_end &&  minHeap_v->size > 0){
        //extract the minimum node from the unvisited node
        running_smallest_vertex_ptr = extractMin(minHeap_v);
        //point to current visiting vertex
        current_vertex =  &(vertexs[running_smallest_vertex_ptr]);
        //point to the adj_list
        node *running_list_ptr = current_vertex->adj_vertex_index_list_head->next;
        //visit all unvisited node
        while(running_list_ptr != NULL){
            
            unsigned long int adj_vertex_index = search_hash(running_list_ptr->val);
            // if it's not visited
            if(vertexs[adj_vertex_index].isVisited == false){
                //relax
                relax(current_vertex, &(vertexs[adj_vertex_index]));
                //heapify to make sure it fulfill the heap property
                upward_heapify(minHeap_v, vertexs, adj_vertex_index);
            }
            //find next adjacent node
            running_list_ptr = running_list_ptr->next;
        }
        //make current vertex to visted
        current_vertex -> isVisited = true;
    }

    //print out result
    print_query(vertexs,unhash_start,unhash_end);

    //free everything
    free_heap(minHeap_v);
    
}
