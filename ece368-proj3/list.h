#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

typedef struct node{
	unsigned long int val;
	struct node * next;
} node;

typedef struct head_tail{
  node *head;
  node *tail;
  unsigned long int length;
} head_tail;

/*
print list
*/
void print_list(node *head);

/*
intitialize the head
*/
head_tail *initialize_head(unsigned long int val);

/*
scan the list and insert if element is not existed 
*/
void scan_and_insert(node *head, unsigned long int val);

/*
Create a node and insert it to the end and update the tail pointer
*/
void insert_Node( head_tail *head_tail_ptr,unsigned long val);

/*
check if the list is empty
*/
bool isEmpty(head_tail *head_tail_ptr);

/*
Remove a node
*/
void remove_node(head_tail *head_tail_ptr, unsigned long val);

/*
free linked lists
*/
void free_all_nodes(node *head);

#endif