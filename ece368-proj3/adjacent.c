
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
unsigned long int size ;

typedef struct node{
	unsigned long int val;
	struct node * next;
} node;

typedef struct head_tail{
  node *head;
  node *tail;
  unsigned long int length;
} head_tail;


typedef struct Vertex{
    unsigned long int x_coor;
    unsigned long int y_coor;
    double shortest_dist;
    signed long int shortest_prev_vertex_index;
    node *adj_vertex_index_list_head;
}Vertex;


void print_list(node *head){
    node *running_ptr = head->next;
    while(running_ptr != NULL){
        printf("%lu ",running_ptr->val);
        running_ptr = running_ptr->next;
    }
    printf("\n");
}




/*
initialize head
*/
head_tail *initialize_head(unsigned long int val){
    node *head = malloc(sizeof(*head));
    head->val = val;
    head->next = NULL;
    head_tail *head_tail_ptr = malloc(sizeof(*head_tail_ptr));
    head_tail_ptr->head = head;
    head_tail_ptr->tail = head;
    head_tail_ptr->length = 1;
    return head_tail_ptr;
}

/*
scan the list and insert if element is not existed 
*/
void scan_and_insert(node *head, unsigned long int val){
        node *running_ptr = head;
        while(running_ptr->next != NULL && running_ptr->next->val != val){
            running_ptr = running_ptr->next;
        }
        if(running_ptr->next == NULL){
            running_ptr->next = malloc(sizeof(node));
            running_ptr->next->val = val;
            running_ptr->next->next = NULL;
        }
}


/*
Create a node and insert it to the end and update the tail pointer
*/
void insert_Node( head_tail *head_tail_ptr,unsigned long int val){
        //allocate memory to the tail->next;
		head_tail_ptr->tail->next = malloc(sizeof(*(head_tail_ptr->tail)));
		if (head_tail_ptr->tail->next == NULL){
			fprintf(stderr, "Error when allocating memory\n" );
			return;
		}
        //set the last linked list to the assigned value
        head_tail_ptr->tail->next->val = val;
        // update next of node to NULL
        head_tail_ptr->tail->next->next = NULL;
        //update the tail to point to the new node
        head_tail_ptr->tail = head_tail_ptr->tail->next;
        //update the length
        head_tail_ptr->length += 1;
        
}

/*
check if the list is empty
*/
bool isEmpty(head_tail *head_tail_ptr){
    //if no element
    if(head_tail_ptr->length == 0){
        return true;
    }
    return false;
}


/*
Remove a node
*/
void remove_node(head_tail *head_tail_ptr, unsigned long int val){
    node *head = head_tail_ptr->head;
    //if it's the first node
    if(head->val == val){
        //update the new head
        head_tail_ptr->head = head->next;
        //update length
        head_tail_ptr->length -= 1;
        //free head
        free(head);
        return;
    }

    node *running_ptr = head; 
    while(running_ptr->next !=NULL && val != running_ptr->next->val){
        running_ptr = running_ptr->next;
    }

    //if it cannot be find
    if(running_ptr->next == NULL){
        return;
    }

    //if it's the end
    if(running_ptr->next->next == NULL){
        //assign the new tail
        head_tail_ptr->tail = running_ptr;
        // free the next node
        free(running_ptr->next);
        // assign current node to NULL
        running_ptr->next = NULL;
        //update length
        head_tail_ptr->length -= 1;
        return;
    }

    //if it's the middle
    node *temp = running_ptr->next;
    running_ptr->next = running_ptr->next->next;
    free(temp);
    head_tail_ptr->length -= 1;
}

/*
free linked lists
*/
void free_all_nodes(node *head){
	node *current_ptr = head;
	node *kill_ptr = NULL;
	while (current_ptr != NULL){
		kill_ptr = current_ptr;
		current_ptr = current_ptr->next;
		free(kill_ptr);
	}
}

void print_vertexs(Vertex *vertexs){

    for (unsigned long int i = 0; i< size; i++){
        printf("%lu: ",i);
        print_list(vertexs[i].adj_vertex_index_list_head);
    }
}

Vertex *build_adjacent(char *input_filename){
    
    unsigned long int num_of_vertex;
    unsigned long int num_of_edges;
    unsigned long int vertex_index;
    unsigned long int vertex_index1;
    unsigned long int x,y;
    //open the file
    FILE *fptr = fopen(input_filename,"r");
    if(fptr == NULL){
        fprintf(stderr,"cannot open input file\n");
    }

    //scan the first two number
    fscanf(fptr, "%lu %lu",&num_of_vertex,&num_of_edges);
    
    //allocate all vertex
    Vertex *vertexs = malloc(num_of_vertex * sizeof(*vertexs));
    size = num_of_vertex;

    // run a for loop until all the coordinates is inserted
  
    for (int i = 0 ; i < num_of_vertex ; i ++){

        fscanf(fptr, "%lu %lu %lu",&vertex_index,&x,&y);
        vertexs[vertex_index].x_coor = x;
        vertexs[vertex_index].y_coor = y;
        vertexs[vertex_index].shortest_dist = INFINITY;
        vertexs[vertex_index].shortest_prev_vertex_index = -1;
        node *head = malloc(sizeof(*head));
        head->next = NULL;
        vertexs[vertex_index].adj_vertex_index_list_head = head;
    }

    // get all edges
    for (int i = 0; i < num_of_edges; i++){
        fscanf(fptr, "%lu %lu",&vertex_index,&vertex_index1);

        Vertex left_vertex = vertexs[vertex_index];
        Vertex right_vertex = vertexs[vertex_index1];

        scan_and_insert(left_vertex.adj_vertex_index_list_head, vertex_index1);
        scan_and_insert(right_vertex.adj_vertex_index_list_head, vertex_index);

    }

    fclose(fptr);
    return vertexs;
}

void free_vertexs(Vertex *vertexes){
    for (int i = 0 ; i < size; i++){
        free_all_nodes(vertexes[i].adj_vertex_index_list_head);
    }
    free(vertexes);
}

int main(int argc, char **argv){

    Vertex *test = build_adjacent(argv[1]);
    print_vertexs(test);
    free_vertexs(test);
    return EXIT_SUCCESS;
}   