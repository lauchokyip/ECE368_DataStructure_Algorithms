


#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "hash.h"
#include "map.h"

void initialize_hash_table(hash *hash_v){
    for(unsigned long int i = 0; i < global_vertex_size; i++){
        hash_v ->hash_table[i] = ULONG_MAX;
    }
}

void print_hash(hash *hash_v){
    for(unsigned long int i = 0; i < global_vertex_size; i++){
        printf("%lu ",hash_v ->hash_table[i] );
    }
    printf("\n");
}

unsigned long int insert_hash(unsigned long int val){
    //use linear probing
    unsigned long int hFn = global_vertex_size;
    unsigned long int n = 0;
    unsigned long int pos =  val % hFn;
    while(hash_v->hash_table[pos] != ULONG_MAX){
        pos = (pos+1)%hFn;
        n++;
        if(n == (global_vertex_size)) break;
    }
    if(n == global_vertex_size){
        fprintf(stderr, "Hash table is full\n");
        return ULONG_MAX;
    }
    else{
        hash_v->hash_table[pos] = val;
    }
    return pos;
}

unsigned long int search_hash(unsigned long int val){
    //use linear probing
    unsigned long int hFn = global_vertex_size;
    unsigned long int n = 0;
    unsigned long int pos =  val % hFn;

    while(n != global_vertex_size){
        if(hash_v->hash_table[pos] == val){
            return pos;
        }
        else{
            pos = (pos+1)%hFn;
            n++;
        }
    }

        return ULONG_MAX;
}

void free_hash(hash *hash_v){
    free(hash_v->hash_table);
    free(hash_v);
}