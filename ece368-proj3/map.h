#ifndef MAP_H
#define MAP_H

#include <stdbool.h>
#include "list.h"
#define NIL -1
unsigned long int global_vertex_size;

typedef struct Vertex{
    unsigned long int self_vertex_index;
    unsigned long int ptr_arr_index;
    signed long int x_coor;
    signed long int y_coor;
    unsigned long int shortest_dist_from_query;
    signed long int shortest_prev_vertex_index;
    bool isVisited;
    node *adj_vertex_index_list_head;
}Vertex;


Vertex *build_adjacent(char *input_filename);

void print_vertexs(Vertex *vertexs);

void dijkstra(Vertex *vertexs, unsigned long int start, unsigned long int end);

void free_vertexs(Vertex *vertexs);

#endif