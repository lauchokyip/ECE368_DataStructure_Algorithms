#include <stdio.h>
#include <stdlib.h>

typedef int bool;
enum {false,true};

typedef struct node{
	int val;
	struct node * next;
	struct node * prev;
} node_t;


node_t *Create_Node(node_t *prev_ptr,int val){
		prev_ptr->next = malloc(sizeof(*prev_ptr));
		if (prev_ptr-> next == NULL){
			fprintf(stderr, "Error when allocating memory\n" );
			return NULL;
		}
		prev_ptr->next->prev = prev_ptr;
	  prev_ptr->next->val = val;
		prev_ptr->next->next = NULL;
		return prev_ptr->next;
	}


void kill_Node(node_t *head){
	node_t *current_ptr = head;
	node_t *kill_ptr = NULL;
	while (current_ptr != NULL){
		kill_ptr = current_ptr;
		current_ptr = current_ptr->next;
		free(kill_ptr);
	}

}

long *Load_File(char *Filename, int *Size){
        FILE *fptr = fopen(Filename,"r");
        if (fptr == NULL)
        {
                fprintf(stderr,"fopen fail\n");
                return NULL;
        }
        int amount;
        int index = 0;
        //read the first
        fscanf(fptr,"%d",&amount);
        // malloc amount of long array

        long *arr = malloc(sizeof(*arr)*amount);
        // malloc fail
        if(arr == NULL) {
                fprintf(stderr,"malloc long int failed");
                return NULL;
        }
        //scan the rest of the documents and store
        while (fscanf(fptr,"%ld",&arr[index])>0) {
                index++;
        }
        //finish
        fclose(fptr);
        *Size = amount;
        return arr;
}

int Save_File (char *Filename,long *Array, int Size){
        FILE *fptr;
        fptr = fopen(Filename,"w");
        if (fptr == NULL) {
                fprintf(stderr,"Open file to write error\n");
                return EXIT_FAILURE;
        }
        //Write the size first
        fprintf(fptr,"%d\n",Size);
        //Then write the next array
				int i;
        for (i= 0; i < Size; i++) {
                fprintf(fptr,"%ld\n",Array[i]);
        }
        // close the pointer
        fclose(fptr);
        return Size;
}


void Shell_Insertion_Sort(long *Array, int Size, double *NComp, double *NMove){
node_t *head = malloc(sizeof(*head));
if (head == NULL){
	return;
}

head->val = 1;
head->next = NULL;
head->prev = NULL;
node_t *tail_ptr = head;
node_t *mul2_ptr = head;
node_t *mul3_ptr = head;

while (tail_ptr-> val < Size){
		int a = 2*(mul2_ptr->val);
		int b = 3*(mul3_ptr->val);

		if (a == b){
			tail_ptr=Create_Node(tail_ptr,a);
			mul2_ptr= mul2_ptr->next;
			mul3_ptr= mul3_ptr->next;
		}
		if (a<b){
			tail_ptr=Create_Node(tail_ptr,a);
			mul2_ptr=mul2_ptr->next;
		}
		if (b<a){
			tail_ptr=Create_Node(tail_ptr,b);
			mul3_ptr=mul3_ptr->next;
		}
}
			tail_ptr = tail_ptr->prev;
			int k = tail_ptr->val;

      while (k>=1){
					int outer;
          for (outer = k; outer < Size; outer++){
            long valueToInsert = Array[outer];
						*NMove= *NMove+1;
            int inner = outer;

            while (inner > k-1 && Array[inner - k] >= valueToInsert){
							*NComp= *NComp +1;
              Array[inner] = Array[inner - k];
							*NMove = *NMove +1;
              inner = inner - k;
            }
						*NComp = *NComp + 1;
            Array[inner] = valueToInsert;
						*NMove = *NMove + 1;
          }
						if (k == 1){
							k = 0;
						}
						else{
							k = tail_ptr->prev->val;
							tail_ptr = tail_ptr->prev;
						}

      }


			kill_Node(head);
}

void Improved_Bubble_Sort (long *Array, int Size, double *NComp, double *NMove){
  int start = 0;
	int end = Size;
	int gap = Size/1.3;
  long temp = 0;
	bool swapped = true;
	while (gap != 1 || swapped == true ){

		swapped = false;
		int i;
		for(i = start; i+gap < end ; i++){
					*NComp = *NComp +1;

			if (Array[i] > Array[i+gap]){
					temp = Array[i+gap];
				 *NMove = *NMove +1;
					Array[i+gap] = Array[i];
				 *NMove = *NMove +1;
					Array[i] = temp;
				 *NMove = *NMove +1;
				 swapped = true;
			}

		}


			if(gap == 9 || gap == 10){
				gap = 11;
			}
			else{
				if (gap == 1){
					gap = 1;
				}
				else{
								gap = gap/1.3;
				}

			}


}

}



void Save_Seq1(char *Filename, int N){
        FILE *fptr;
        fptr = fopen(Filename,"w");
        if (fptr == NULL) {
                fprintf(stderr, "Open file to write in sequence failed\n" );
                return;
        }
				node_t *head = malloc(sizeof(*head));
				if (head == NULL){
					return;
				}

				fprintf(fptr,"%d\n",N);


				head->val = 1;
				head->next = NULL;
				head->prev = NULL;
				node_t *tail_ptr = head;
				node_t *mul2_ptr = head;
				node_t *mul3_ptr = head;

				while (tail_ptr-> val < N){
						int a = 2*(mul2_ptr->val);
						int b = 3*(mul3_ptr->val);

						if (a == b){
							tail_ptr=Create_Node(tail_ptr,a);
							mul2_ptr= mul2_ptr->next;
							mul3_ptr= mul3_ptr->next;
						}
						if (a<b){
							tail_ptr=Create_Node(tail_ptr,a);
							mul2_ptr=mul2_ptr->next;
						}
						if (b<a){
							tail_ptr=Create_Node(tail_ptr,b);
							mul3_ptr=mul3_ptr->next;
						}
				}

				node_t *print_ptr = head;
				node_t *kill_ptr = NULL;
				while (print_ptr != NULL && (print_ptr->val) <N){
					kill_ptr = print_ptr;
					fprintf(fptr,"%d\n",print_ptr->val);
					print_ptr = print_ptr->next;
					free(kill_ptr);
				}
			free(print_ptr);

        fclose(fptr);
   }

void Save_Seq2(char *Filename, int N){
  FILE *fptr;
  fptr = fopen(Filename,"w");
  if (fptr == NULL) {
          fprintf(stderr, "Open file to write in sequence failed\n" );
          return;
  }
  int seq = N;
  while (seq > 1){
    if(seq == 9 || seq == 10){
      seq = 11;
    }
		else {
			  seq = seq/1.3;
		}

    fprintf(fptr,"%d\n",seq);
  }

  fclose(fptr);
}
