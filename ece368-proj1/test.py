
def is_smooth(n):
    number = n
    while (number%2 == 0 or number%3 == 0):
        if(number %2 == 0):
            number = number/2
        if(number %3 == 0):
            number = number/3
    if (number == 1):
        return True
    if (number >1):
        return False

def generate_data(n):
    i = 1
    data = []
    while i <= n:
        data.append(i)
        i = i+1
    return data
def main():
    data = generate_data(3889)
    output = []
    output = [x for x in data if is_smooth(x) == True]
    print output


if __name__ == "__main__":
    main()
