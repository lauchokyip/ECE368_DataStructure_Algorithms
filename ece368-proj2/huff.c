#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "huff.h"
#include "list.h"


////////////////////////////////////////////

void write_bit(FILE *fptr, unsigned char bit, unsigned char *current_byte, unsigned char *bitcount){

    *current_byte = *current_byte << 1 | bit;
    *(bitcount) += 1;

    if(*bitcount == 8){
      fprintf(fptr,"%c",*current_byte);
      *current_byte = 0;
      *bitcount = 0;
    }

}

void write_character(FILE *fptr, unsigned char character, unsigned char *current_byte, unsigned char *bitcount){
    unsigned char mask = 0x80;
    while( (int)mask > 0){
      unsigned char temp = character & mask;
      write_bit(fptr,temp==mask,current_byte,bitcount);
      mask >>= 1;
    }
}

///////////////////////////////////////////////

int count_frequency (char *filename, char_freq *char_arr){
  FILE *fptr = fopen(filename, "r");
  if(fptr == NULL){
    fprintf(stderr,"File cannot be opened");
    return EXIT_FAILURE;
  }

  //initialize every char_arr to zero
  for (int i = 0; i < MAX_ASCII_VALUE; i++){
    char_arr[i].freq = 0;
  }

  char temp;
  while(fscanf(fptr, "%c", &temp) != EOF){
    int ascii_val = temp;
    char_arr[ascii_val].value = (char) ascii_val;
    char_arr[ascii_val].freq ++;
  }

  fclose(fptr);
 return EXIT_SUCCESS;
}


void print_frequency(char_freq *char_arr){
  int sum = 0;
  for (int i=0 ; i < MAX_ASCII_VALUE ; i ++){
    printf(" The charecter %c has the  freqenct of %d\n",char_arr[i].value, char_arr[i].freq);
    sum += char_arr[i].freq;
  }
    printf("Sum of frequency is %d, ", sum);
}

int compare(const void *p1, const void *p2){
  int left = ((char_freq *)p1)->freq;
  int right = ((char_freq *)p2)->freq;
  return (left - right);
}

void sort_frequency(char_freq *char_arr){
  qsort (char_arr, MAX_ASCII_VALUE, sizeof(*char_arr), compare);
}

tree_node *create_tree_node(int freq, unsigned char value){
  tree_node *temp = malloc(sizeof(*temp));
  temp->freq = freq;
  temp->value = value;
  temp->left_node = NULL;
  temp->right_node = NULL;
  return temp;
}

///////////////////////////////////

listnode *merge_two_tree_node(listnode *head_node){
  if (head_node == NULL){
    fprintf(stderr,"head node is not correct!\n");
    return NULL;
  }
 tree_node *temp_tree_node = create_tree_node(head_node->tree->freq + head_node->next->tree->freq,(char)0);
 if (head_node->tree->freq <= head_node->next->tree->freq){
   temp_tree_node->left_node = head_node->tree;
   temp_tree_node->right_node = head_node->next->tree;
 }
 else{
   temp_tree_node->right_node = head_node->tree;
   temp_tree_node->left_node = head_node->next->tree;
 }
 listnode  *temp_list_node = create_list_node(temp_tree_node);

 // if there is only two list left
 if (head_node->next->next == NULL){
       listnode *new_head = temp_list_node;
       free(head_node->next);
       free(head_node);
       return new_head;
 }

 listnode *new_head = head_node->next->next;
 listnode *moving_head = new_head;

 // if temp_list_node is the head
 if (temp_list_node->tree->freq <= new_head->tree->freq){
     new_head = temp_list_node;
     new_head -> next = head_node->next->next;
     free(head_node->next);
     free(head_node);
     return new_head;
 }

 while (moving_head->next != NULL &&  moving_head->next->tree->freq < temp_list_node->tree->freq){
// if it is in the end
     moving_head = moving_head->next;
 }

 if (moving_head -> next == NULL){
   moving_head->next = temp_list_node;
   free(head_node->next);
   free(head_node);
   return new_head;
 }

listnode *temp = moving_head -> next;
moving_head->next  = temp_list_node;
temp_list_node -> next = temp;



free(head_node->next);
free(head_node);
 return new_head;
}

///////////////////////////////////

tree_node *build_tree(listnode *head_node){
 while(head_node -> next != NULL){
      head_node = merge_two_tree_node(head_node);
 }

 tree_node *temp = head_node->tree;
 free(head_node);
 //return
 return temp;
}

///////////////////////////////////

void print_tree(tree_node *root){
  if(root->left_node == NULL && root -> right_node == NULL){
    printf("The %c has frequency %d \n",root->value,root->freq);
    return;
  }
  print_tree(root->left_node);
  print_tree(root->right_node);
}

////////////////////////////////////


void free_tree(tree_node *root){
  if(root == NULL){
    return;
  }
  free_tree(root->left_node);
  free_tree(root->right_node);
  free(root);
}

///////////////////////////////////////
void find_max_tree_height_helper(tree_node *root, int len,int *max_height){

  if (root == NULL){
    fprintf(stderr,"no root\n");
    return;
  }
 if (root ->left_node == NULL && root ->right_node == NULL){
   if(len > *max_height){
     *max_height = len;
   }
    return ;
 }
    find_max_tree_height_helper(root->left_node,len+1,max_height);
    find_max_tree_height_helper(root->right_node,len+1,max_height);
}

void find_max_tree_height(tree_node *root, int *max_height){
  find_max_tree_height_helper(root,0,max_height);
}
////////////////////////////////////////
void find_max_tree_leaf_node(tree_node *root,int *num_of_node){

  if (root == NULL){
    fprintf(stderr,"no root\n");
    return;
  }
 if (root ->left_node == NULL && root ->right_node == NULL){
    *(num_of_node) +=1;
    return ;
 }
    find_max_tree_leaf_node(root->left_node,num_of_node);
    find_max_tree_leaf_node(root->right_node,num_of_node);
}


////////////////////////////////////////

int** build_table(tree_node *root){
 int max_row = 0;
 int max_column = 0;
 find_max_tree_height(root,&max_column);
 find_max_tree_leaf_node(root,&max_row);

 //consider the first column
 max_column+=1;

 int **table = malloc(sizeof(*table) * max_row);
 if (table == NULL){
       fprintf(stderr,"malloc failed in build_table\n");
       return NULL;
 }
 for (int i = 0 ; i < max_row ; i ++){
   table[i] = malloc(sizeof(int) * (max_column));
   if (table[i] == NULL){
         fprintf(stderr,"malloc failed in build_table\n");
         return NULL;
   }
   for (int j = 0; j < max_column ; j++){
     table[i][j] = -1;
   }
 }

 int row=0;
 build_table_helper(root, table,&row,1);
 return table;
}

void build_table_helper(tree_node *root, int **arr,int *row,int column){
  if (root == NULL){
    fprintf(stderr,"no root\n");
    return;
  }
if (root ->left_node == NULL && root ->right_node == NULL){
    arr[*row][0] = root->value;
  (*row) += 1;
}
 if(root->left_node != NULL){
     int num_of_leaf_node=0;
    find_max_tree_leaf_node(root->left_node,&num_of_leaf_node);
     for(int i =  *row ; i < *row + num_of_leaf_node  ; i ++){
            arr[i][column] = 0;
     }
     build_table_helper(root->left_node,arr,row,column+1);
 }

 if(root->right_node != NULL){
   int num_of_leaf_node=0;
   find_max_tree_leaf_node(root->right_node,&num_of_leaf_node);
   for(int i =  *row ; i <  *row + num_of_leaf_node  ; i ++){
          arr[i][column] = 1;
   }
     build_table_helper(root->right_node,arr,row,column+1);
 }
}

void print_table(int **arr, int maxRow, int max_column){
  for (int row = 0; row < maxRow; row++){
    printf("%c : ", arr[row][0]);
    int i = 1;
    while (arr[row][i] != -1 && i < max_column){
        printf("%d ", arr[row][i]);
        i++;
    }
    printf("\n");
  }

}
//////////////////////////////////////////////////


void build_header_helper(tree_node*root , FILE *fptr,unsigned char *current_byte,unsigned char *bitcount){
  if(root == NULL){
    fprintf(stderr,"In build header there is no root exists\n");
    return;
  }
  if(root->left_node == NULL && root->right_node == NULL){
      // fprintf(fptr,"1%d",(unsigned int)root->value);
     write_bit(fptr,1,current_byte,bitcount);
     write_character(fptr,root->value,current_byte,bitcount);
    return;
  }
  // fprintf(fptr,"0");
   write_bit(fptr,0,current_byte,bitcount);
  build_header_helper(root->left_node,fptr,current_byte,bitcount);
  build_header_helper(root->right_node,fptr,current_byte,bitcount);
}

//////////////

void build_header(tree_node*root , char *output_filename){
  FILE *fptr = fopen(output_filename,"w");
  if(fptr == NULL){
    fprintf(stderr,"File cannot be opened");
    return;
  }

  unsigned char current_byte = 0;
  unsigned char bitcount = 0;

  build_header_helper(root,fptr,&current_byte,&bitcount);
  // fprintf(fptr,"1%d",(int)PSEUDO_EOH);
   write_bit(fptr,1,&current_byte,&bitcount);
   write_character(fptr,(unsigned char)PSEUDO_EOH,&current_byte,&bitcount);

   while(bitcount!=0){
     write_bit(fptr,0,&current_byte,&bitcount);
   }

  fclose(fptr);
}


///////////////////////////////////////////////////

int **mapping(int **table, int max_row){
  int **arr_of_ptr = malloc(sizeof(*arr_of_ptr) * (PSEUDO_EOF+1));
  if (arr_of_ptr == NULL){
    fprintf(stderr, "malloc failed in mapping\n");
  }

  // run a loop in the builded table
  for (int i= 0 ; i < max_row ; i++){
    int ascii_val = (int)table[i][0];
    arr_of_ptr[ascii_val] = table[i];
  }
  return arr_of_ptr;
}


void scan_file_convert_to_huffman_code(char *input_filename,char *output_filename, int** arr_of_ptr, int max_column){

 unsigned char current_byte = 0;
 unsigned char bitcount = 0;

  FILE *in_fptr = fopen(input_filename,"r");
  if(in_fptr == NULL){
    fprintf(stderr,"File cannot be opened");
    return;
  }
  FILE *out_fptr = fopen(output_filename,"a");
  if(out_fptr == NULL){
    fprintf(stderr,"File cannot be opened");
    return;
  }

  char temp;
  while(fscanf(in_fptr, "%c", &temp)!=EOF){
    int ascii_val = (int)temp;
    int *ptr_to_arr = arr_of_ptr[ascii_val];
    int i = 1;
    while(i< max_column && ptr_to_arr[i] != -1  ){
    //  fprintf(out_fptr,"%d",ptr_to_arr[i]);// use this to debug
      write_bit(out_fptr, ptr_to_arr[i],&current_byte,&bitcount);
      i++;
    }
  }
 // write EOF path
 int i =1;
 while(i< max_column && arr_of_ptr[PSEUDO_EOF][i] != -1  ){
   write_bit(out_fptr, arr_of_ptr[PSEUDO_EOF][i],&current_byte,&bitcount);
   i++;
 }


  while(bitcount!=0){
    write_bit(out_fptr,0,&current_byte,&bitcount);
  }

  //free both array



 fclose(in_fptr);
 fclose(out_fptr);
}

void freeArray(int **arr, int max_row){
 for (int i = 0 ; i < max_row; i++){
   free(arr[i]);
 }
 free(arr);
}

///////////////////////////////////////////////////


void encode(char *input_filename){
  //concanate string
  char output_filename[50];
  strcpy(output_filename,input_filename);
  strcat(output_filename,".huff");
  // malloc array of frequency
  char_freq *arr_freq = malloc( MAX_ASCII_VALUE * sizeof(*arr_freq));
  // count array of frequency
  count_frequency(input_filename,&arr_freq[0]);
  // sort the array of frequency based on frequency
  sort_frequency(&arr_freq[0]);
  // convert array of frequency to list
  listnode *head = charArr_to_list(&arr_freq[0]);
  // build huffman tree
  tree_node* root = build_tree(head);
  //build the table for encoding
  int **table = build_table(root);

  // find the max_row and max_column
  int max_row = 0;
  int max_column = 0;
  find_max_tree_height(root,&max_column);
  find_max_tree_leaf_node(root,&max_row);
  //consider the first element
   max_column++;
  // build the header
  build_header(root,output_filename);
  // free the tree
  free_tree(root);
  // map the table to array of pointer
  int **map = mapping(table, max_row);
  // output it to the file
  scan_file_convert_to_huffman_code(input_filename,output_filename,map,max_column);

  freeArray(table,max_row);
  free(map);
}
