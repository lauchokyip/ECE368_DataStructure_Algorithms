#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "huff.h"
#include "list.h"
void readbit(FILE *fptr,unsigned char *bit,unsigned char *current_byte, unsigned char *bitcount){

  if(*bitcount == 0){
    fscanf(fptr,"%c",current_byte);
  }

   unsigned char temp= *current_byte >> (7-*(bitcount)) ;
   temp &= 0x1;
   *bit = temp;

   *(bitcount) += 1;

  if(*bitcount == 8){
    *current_byte = 0;
    *bitcount = 0;
  }
}

void get_char(FILE *fptr,unsigned char *character, unsigned char *current_byte,unsigned char *bitcount){

  unsigned char bit = 0;
  *character = 0;

  for (int i = 0; i < 8 ; i++){
    if(*bitcount == 8){
      *current_byte = 0;
      *bitcount = 0;
    }
    *character <<= 1;
    readbit(fptr, &bit,current_byte,bitcount);
    *character |= bit;
  }
}
/////////////////////////////////////////////////////////


void print_readbit(char *input_filename, char *output_filename){
  FILE *in_fptr = fopen(input_filename,"r");
  if (in_fptr == NULL){
    fprintf(stderr, "File opened error\n");
    return;
  }
  FILE *out_fptr = fopen(output_filename,"w");
  if (in_fptr == NULL){
    fprintf(stderr, "File opened error\n");
    return;
  }
  unsigned char current_byte=0;
  unsigned char bitcount=0;
  unsigned char bit = 0;
  for (int i = 0; i < 10000; i++){
    readbit(in_fptr,&bit,&current_byte,&bitcount);
    fprintf(out_fptr,"%d",bit);
  }
  fclose(in_fptr);
  fclose(out_fptr);
}


///////////////////////////////////////////////

void print_encode_header(char *input_filename, char *output_filename){
    FILE *in_fptr = fopen(input_filename,"r");
    if(in_fptr == NULL){
      fprintf(stderr, "cannot opened file in print encode header\n" );
      fclose(in_fptr);
      return;
    }
    FILE *out_fptr = fopen(output_filename,"w");
    if(out_fptr == NULL){
      fprintf(stderr, "cannot opened file in print encode header\n" );
      fclose(out_fptr);
      return;
    }

    unsigned char bit = 0;
    unsigned char current_byte=0;
    unsigned char bitcount=0 ;
    unsigned char running_character=0;

    while(running_character != PSEUDO_EOH){
      readbit(in_fptr,&bit,&current_byte,&bitcount);
      if(bit == 1){
        get_char(in_fptr,&running_character,&current_byte,&bitcount);
        fprintf(out_fptr,"%d\n",running_character);
      }
    }
    fclose(in_fptr);
    fclose(out_fptr);
}
////////////////////////////////////////////////////////

tree_node *build_decode_tree_helper(FILE *fptr, unsigned char *current_byte,unsigned char *bitcount){

      tree_node *temp = create_tree_node(0,0);
      unsigned char bit = 0;
      readbit(fptr,&bit,current_byte,bitcount);

      if(bit == 0){
        temp->left_node = build_decode_tree_helper(fptr,current_byte,bitcount);
        temp->right_node = build_decode_tree_helper(fptr,current_byte,bitcount);
      }
      if(bit == 1){
        unsigned  char running_character = 0;
        get_char(fptr,&running_character,current_byte,bitcount);
        temp->value = running_character ;
      }
        return temp;
}

tree_node *build_decode_tree( FILE *fptr,char *input_filename){

  unsigned char current_byte=0;
  unsigned char bitcount=0 ;
  unsigned char bit = 1;
  unsigned char running_character =0 ;
  readbit(fptr,&bit,&current_byte,&bitcount);
  if(bit == 1 ){
    fprintf(stderr, "First bit should not be 1\n");
    return NULL;
  }
      tree_node *root = create_tree_node(0,0);
      root ->left_node = build_decode_tree_helper(fptr,&current_byte,&bitcount);
      root ->right_node =build_decode_tree_helper(fptr,&current_byte,&bitcount);

      readbit(fptr,&bit,&current_byte,&bitcount);
      if(bit == 1 ){
        get_char(fptr,&running_character,&current_byte,&bitcount);
        if(running_character !=PSEUDO_EOH){
          fprintf(stderr, "Did not reach end of header\n");
          return NULL;
        }
      }
      return root;

}


////////////////////////////////////////////////////
void build_and_print_from_tree(char *input_filename,char *output_filename){
  FILE *in_fptr = fopen(input_filename,"r");
  if(in_fptr == NULL){
    fprintf(stderr, "cannot opened file in print encode header\n" );
    fclose(in_fptr);
    return;
  }
  FILE *out_fptr = fopen(output_filename,"w");
  if(out_fptr == NULL){
    fprintf(stderr, "cannot opened file in print encode header\n" );
    fclose(out_fptr);
    return;
  }
  tree_node *root = build_decode_tree(in_fptr,input_filename);

  unsigned char current_byte=0;
  unsigned char bitcount=0 ;
  unsigned char bit = 0;

  while(1){
    tree_node *temp = root;
    while(temp ->left_node != NULL && temp -> right_node != NULL){
      readbit(in_fptr,&bit,&current_byte,&bitcount);
      if(bit == 0){
        temp = temp->left_node;
      }
      else{
        temp = temp->right_node;
      }

    }
    if(temp-> value == PSEUDO_EOF){
      break;
    }
    fprintf(out_fptr,"%c",temp->value);
  }


  fclose(in_fptr);
  fclose(out_fptr);
  free_tree(root);
}

//////////////////////////////////////////////////////
void decode(char *input_filename){
  //concanate string
  char output_filename[50];
  strcpy(output_filename,input_filename);
  strcat(output_filename,".unhuff");
  build_and_print_from_tree(input_filename,output_filename);

}
