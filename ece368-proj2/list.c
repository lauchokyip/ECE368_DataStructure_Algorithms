#include <stdio.h>
#include <stdlib.h>
#include "huff.h"
#include "list.h"


listnode *create_list_node( struct tree_node *ptr){
  listnode *temp = malloc(sizeof(*temp));
  if (temp == NULL){
    fprintf(stderr, "Error when allocating memory\n" );
    return NULL;
  }
  temp -> tree  = ptr;
  temp -> next = NULL;
  return temp;
}


listnode *charArr_to_list(struct char_freq *char_arr){
  int index = 0;
  while (char_arr[index].freq == 0){
    index++;
}

 listnode *temp_head = create_list_node(create_tree_node(1,(unsigned char)PSEUDO_EOF));
 listnode *moving_ptr = temp_head;

 while (index < MAX_ASCII_VALUE){
   moving_ptr->next = create_list_node(create_tree_node(char_arr[index].freq, char_arr[index].value));
   moving_ptr = moving_ptr->next;
   index++;
 }
 free(char_arr);
 return temp_head;
}


void print_list_node(listnode *head){
  while (head != NULL){
    printf("the character %c has frequency %d \n",head->tree->value,head->tree->freq);
    head = head->next;
  }
}
