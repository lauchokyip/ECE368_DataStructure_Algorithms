#ifndef __HUFF_H
#define __HUFF_H

#define MAX_ASCII_VALUE 128
#define PSEUDO_EOF 129
#define PSEUDO_EOH 130

#include <stdio.h>
#include "list.h"




/*
intput: filename, array of char_freq
output: true if read file successfully, otherwsie false
function: count the frequency of each character
*/
int count_frequency (char *filename, char_freq * char_arr);

/*
intput: array of char_freq
output: none
function: print frequency of each character
*/
void print_frequency(char_freq * char_arr);

/*
intput: array of char_freq
output: none
function: sort each character using qsort
*/
void sort_frequency(char_freq *char_arr);

/*
intput: array of char_freq
output: pointer to root
function: build the tree and free the input array
*/
void write_bit(FILE *fptr, unsigned char bit, unsigned char *current_byte, unsigned char *bitcount);
void write_character(FILE *fptr,  unsigned char character, unsigned char *current_byte, unsigned char *bitcount);
tree_node *create_tree_node(int freq, unsigned char value);
listnode *merge_two_tree_node(listnode *head_node);
tree_node *build_tree(listnode *head_node);
void print_tree(tree_node *root);
void free_tree(tree_node *root);
void find_max_tree_height(tree_node *root, int *max_height);
void find_max_tree_leaf_node(tree_node *root,int *num_of_node);
int** build_table(tree_node *root);
void build_table_helper(tree_node *root, int **arr,int *row,int column);
void print_table(int **arr, int maxRow, int max_column);
void build_header_helper(tree_node*root , FILE *fptr,unsigned char *current_byte,unsigned char *bitcount);
void build_header(tree_node*root , char *output_filename);
void encode(char *input_filename);
int **mapping(int **table, int max_row);
void scan_file_convert_to_huffman_code(char *input_filename,char *output_filename, int** arr_of_ptr, int max_column);

#endif
