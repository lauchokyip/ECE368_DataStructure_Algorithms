#ifndef __LIST_H
#define __LIST_H



typedef struct listnode
{
  struct listnode *next;
  struct tree_node * tree;
} listnode;

typedef struct char_freq{
  char value ;
  int freq ;
} char_freq;

typedef struct tree_node{
  unsigned char value;
  int freq;
  struct tree_node *left_node;
  struct tree_node *right_node;
} tree_node;

void print_list_node(listnode *head);
listnode *charArr_to_list(char_freq *char_arr);
listnode *create_list_node(tree_node *ptr);


#endif
