#ifndef __UNHUFF_H
#define __UNHUFF_H
#include <stdio.h>
#include "list.h"
#include "huff.h"

void readbit(FILE *fptr,unsigned char *bit,unsigned char *current_byte, unsigned char *bitcount);
void get_char(FILE *fptr,unsigned char *character, unsigned char *current_byte,unsigned char *bitcount);
void print_readbit(char *input_filename, char *output_filename);
void decode(char *input_filename);
void print_encode_header(char *input_filename, char *output_filename);
tree_node *build_decode_tree_helper(FILE *fptr, unsigned char *current_byte,unsigned char *bitcount);
tree_node *build_decode_tree( FILE *fptr,char *input_filename);
void build_and_print_from_tree(char *input_filename,char *output_filename);
#endif
