#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "unhuff.h"
#include "list.h"
#include "huff.h"


int main (int argc, char **argv){
  encode(argv[1]);
  char output_filename[50];
  strcpy(output_filename,argv[1]);
  strcat(output_filename,".huff");
  decode(output_filename);


  return EXIT_SUCCESS;
}
